package main

import (
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}

	e := echo.New()

	setupHandler(e)

	e.Start(":4000")
}

func setupHandler(e *echo.Echo) {
	e.GET("/FOO", func(c echo.Context) error {
		return c.String(200, "BAR")
	})

	e.GET("/info", info)
}

func info(c echo.Context) error {
	return c.JSON(200, echo.Map{
		"VERSION": "v" + os.Getenv("VERSION"),
	})
}
